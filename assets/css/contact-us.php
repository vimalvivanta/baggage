<?php 
  include("assets/config.php"); 
  include(INC."header.php");

     require_once(CONTROLLER.'AdminController.php');

  $admincontroller = new AdminController();

  $storedetails=$admincontroller->getStoreDetails();
      require_once(CONTROLLER.'AdminController.php');
  $bannercontroller = new AdminController();
  $bannerslides=$bannercontroller->retAllBanners("contact-us");

 ?>

<div class="row"> <img src="<?php echo WWWROOT; ?>assets/<?php echo $bannerslides[0]['image_url'];?>" class="img-responsive" alt="Image"> </div>
<section class="contact-detils padtb60">
  <div class="container">
    <div class="col-md-1"> <img src="<?php echo IMAGES; ?>contact1.png" class="img-responsive center-block" alt="Image"> </div>
    <div class="col-md-2 border-right">
      <h3>Corporate Office</h3>
      <address>
        <?php echo $storedetails['address_line_2']; ?>
      </address>
    </div>
    <div class="col-md-1"> <img src="<?php echo IMAGES; ?>contact2.png" class="img-responsive center-block" alt="Image"> </div>
    <div class="col-md-2 border-right">
      <h3>Contact Us</h3>
         <address>
          <i class="fa fa-phone-square" aria-hidden="true"></i> 
          <a href="tel:<?php echo $storedetails['contact_no_1']; ?>"> <?php echo $storedetails['contact_no_1']; ?></a><br>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:<?php echo $storedetails['contact_no_2']; ?>"> <?php echo $storedetails['contact_no_2']; ?></a>
          <br>
         <i class="fa fa-envelope-o" aria-hidden="true"></i>
            
          <a href="mailto:<?php echo $storedetails['contact_email_1']; ?>"> <?php echo $storedetails['contact_email_1']; ?> </a><a href="mailto:<?php echo $storedetails['contact_email_2']; ?>"> <?php echo $storedetails['contact_email_2']; ?> </a>
          </address>
      
    </div>
<div class="col-md-1"> <img src="<?php echo IMAGES; ?>contact3.png" class="img-responsive center-block" alt="Image"> </div>
    <div class="col-md-2 border-right">
      <h3>Development Office</h3>
      <address>
       <?php echo $storedetails['address_line_1']; ?>
      </address>
    </div>
    <div class="col-md-1"> <img src="<?php echo IMAGES; ?>contact3.png" class="img-responsive center-block" alt="Image"> </div>
    <div class="col-md-2 border-right">
      <h3>U.S Office</h3>
      <address>
       <?php echo $storedetails['address_line_3']; ?> 
      </address>
    </div>
  </div>
</section>

<!-- 
=======================
CONATCT FORM  SECTION
=======================
 -->
<section>
  <div class=""> 
    <!-- content start -->
    <div class="container">
      <div class="col-md-6">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.9813675268597!2d77.599918!3d12.908919!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3e266b0a9ab1%3A0x4159e6034afac2c8!2sTechnosphere+Labs+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1488284637923" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="col-md-6 form-custom" id="header-enq">
        <div class="col-md-12">
          <div class="alert alert-success" id="quick-contact-success_message" style="display: none"> <strong>Your Message was successfully sent!</strong> </div>
        </div>
       
          <form id="quick-contact-form" role="form" action="" method="post" enctype="multipart/form-data">
          <div class=" "> 
            <!-- Text input-->
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                <label class="sr-only control-label" for="name">name<span class=" "> </span></label>
                <input id="name" name="name" type="text" required="" placeholder="Name" class="form-control input-md" >
              </div>
            </div>
            <!-- Text input-->
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                <label class="sr-only control-label" for="email">Email<span class=" "> </span></label>
                <input id="email" name="email" type="email" required="" placeholder="Email" class="form-control input-md" >
              </div>
            </div>
            <!-- Text input-->
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                <label class="sr-only control-label" required="" for="phone">Phone<span class=" "> </span></label>
                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control input-md" >
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                <label class="sr-only control-label" required="" for="phone">Service<span class=" "> </span></label>
                <input id="Service" name="Service" type="text" placeholder="Service" class="form-control input-md" >
              </div>
            </div>
            <!-- Select Basic -->
            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label" for="message"> </label>
                <textarea class="form-control" id="message" rows="7" name="message" placeholder="Message"></textarea>
              </div>
            </div>
            <div class="col-md-12 col-xs-12">
         <!--      <div class="form-group">
                <script src='https://www.google.com/recaptcha/api.js'></script>
<div class="g-recaptcha" data-sitekey="6LdjSBcUAAAAAB-_Vfqr1y1Tyxqi2wlr3tr5VUVi"></div>
            </div> -->
            <!-- Button -->
            <div class="">
                 <button id="banner-upload-btn" name="banner-upload-btn" type="button" class="btn btn-primary" onclick="sendMail(this.id)">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="<?php echo JS.'pagecontent-js.js?ver='.filemtime("assets/js/contact-us.js").''; ?>"></script>
<?php include(INC."footer.php"); ?>
