    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#dv_'+input.id).attr('src', e.target.result);                
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#profileimg").change(function(){
        readURL(this);
    });

/*Add Admin By Super Admin*/
function addblog(id) 
{   

    if($('#blog-title').val() ==""){
        $('#error-blog-title').html("Please enter the blog title.");
        return false;
     }else{
        $('#error-blog-title').html("");
     }

    if($('#blog-text').val() ==""){
        $('#error-blog-text').html("Please enter the blog text.");
        return false;
     }else{
        $('#error-blog-text').html("");
     }

      var blogimage=$('#blogimage').val();
      

    // var fd = new FormData();    
    // fd.append("form", id);
    // fd.append('title',$('#blog-title').val());
    // fd.append('text',$('#blog-text').val());
    // fd.append('blogimage',blogimage);
    var fd = new FormData($("#create-blog-form")[0]);
    fd.append("form", id);
    //alert(fd);
    // var event_content=CKEDITOR.instances.blog-text.getData();
    // fd.append('event_content',event_content);

    $.ajax({
      url: getBaseURL()+'assets/handler/BlogsHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
    })
    .done(function(res) {
        console.log(res);
        //alert(res);
        if(res==1)
        {
            window.location.href = "blogs-list";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


/*Change blog Status*/
function blogStatus(id,status) {
 //alert(user_id);
      if(status==0) {
        $('#user-activate-'+id).removeClass('hide');
        $('#user-inactivate-'+id).addClass('hide');
        $('#user_status-'+id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+id).html('<span style="color:red">Inactive</span>');
      }
      else{
        $('#user-activate-'+id).addClass('hide');
        $('#user-inactivate-'+id).removeClass('hide');
        $('#user_status-'+id).html('<span class="green">Active</span>');
        $('#userstatus-'+id).html('<span style="color:green">Active</span>');
      }

      var fd = new FormData();
      fd.append('status', status);       // for status
      fd.append('id', id);
      fd.append("form", "BlogStatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/BlogsHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        //alert(res);
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}


/*Add Admin By Super Admin*/
function addmember(id) 
{   


    
    // var fd = new FormData();    
    // fd.append("form", id);
    // fd.append('title',$('#blog-title').val());
    // fd.append('text',$('#blog-text').val());
    // fd.append('blogimage',blogimage);
    var fd = new FormData($("#create-member-form")[0]);
    fd.append("form", "addmember");
    //alert(fd);
    var brief_info=CKEDITOR.instances.brief_info.getData();
    fd.append('brief_info',brief_info);

    $.ajax({
      url: getBaseURL()+'assets/handler/MemberHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
    })
    .done(function(res) {
        console.log(res);
        //alert(res);
        if(res==1)
        {
            window.location.href = "members-list";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


/*Change blog Status*/
function memberStatus(id,status) {
 // alert(id);
      if(status==0) {
        $('#user-activate-'+id).removeClass('hide');
        $('#user-inactivate-'+id).addClass('hide');
        $('#user_status-'+id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+id).html('<span style="color:red">Inactive</span>');
      }
      else{
        $('#user-activate-'+id).addClass('hide');
        $('#user-inactivate-'+id).removeClass('hide');
        $('#user_status-'+id).html('<span class="green">Active</span>');
        $('#userstatus-'+id).html('<span style="color:green">Active</span>');
      }

      var fd = new FormData();
      fd.append('status', status);       // for status
      fd.append('id', id);
      fd.append("form", "memberstatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/MemberHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        //alert(res);
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}