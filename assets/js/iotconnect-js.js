    function readURL(input) {




        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#dv_'+input.id).attr('src', e.target.result);                
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#about_image").change(function(){
        readURL(this);
    });

    $("#mission_image").change(function(){
        readURL(this);
    });


        $("#block_images").change(function(){
        readURL(this);
    });

               $("#block_imagen").change(function(){
        readURL(this);
    });

                      $("#blocks_image").change(function(){
        readURL(this);
    });

                             $("#block_image").change(function(){
        readURL(this);
    });
        




/*Update Social Links*/
function updateWhoweare(id) 
{   

    var fd = new FormData($("#create-homepage-for")[0]);
  
    
      var who_we_are=CKEDITOR.instances.who_we_are.getData();


      fd.append('who_we_are',who_we_are);
      fd.append("form", "homepagecontent");

    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function updateBlockone(id) 
{   


    var fd = new FormData($("#blockone")[0]);
   
      var block_content=CKEDITOR.instances.block_content_one.getData();


      fd.append('block_content',block_content);
    fd.append("form", "homepagecontent");
    

    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function updateBlocktwo(id) 
{   


    var fd = new FormData($("#blocktwo")[0]);

     var block_content_two=CKEDITOR.instances.block_content_two.getData();


      fd.append('block_content',block_content_two);
    fd.append("form", "homepagecontent");
    
    
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


function updateBlockthree(id) 
{   


    var fd = new FormData($("#blockthree")[0]);

     var block_content_three=CKEDITOR.instances.block_content_three.getData();


      fd.append('block_content',block_content_three);
    fd.append("form", "homepagecontent");
    
    
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function updateBlockfour(id) 
{   


    var fd = new FormData($("#blockfour")[0]);
         var block_content_four=CKEDITOR.instances.block_content_four.getData();


      fd.append('block_content',block_content_four);
    fd.append("form", "homepagecontent");
    
    
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}




function updatequestion(id) 
{   

    var fd = new FormData($("#questionform")[0]);
  
    
      var question=CKEDITOR.instances.question.getData();
      fd.append('question',question);

        fd.append("form", "homepagecontent");
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}
/*Update aboutus*/
function updateAboutus(id) 
{

    var fd = new FormData($("#create-aboutuspage-form")[0]);
    
    
    var about_text=CKEDITOR.instances.about_text.getData();
    var mission_text=CKEDITOR.instances.mission_text.getData();
    fd.append('about_text',about_text);
    fd.append('mission_text',mission_text);
    fd.append("form", "aboutuspagecontent");
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Update product description*/
function updateproductdesc(id) 
{

    var fd = new FormData($("#create-productdesc-form")[0]);
    
    var desctext=CKEDITOR.instances.description.getData();
    fd.append('description',desctext);
    fd.append("form", "productdesccontent");
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,
        beforeSend: function(){
         $("#loading").show();
       },
       complete: function(){
         $("#loading").hide();
       },   
    })
    .done(function(res) {
        //alert(res);
        if(res==1)
        {
          $('#update-response-message').html('Updated Successfully');
          window.location="iot_connectivity";
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Update product description*/
function updateblogmodelistic(id) 
{

    var fd = new FormData($("#create-productdesc-form")[0]);
    
    var desctext=CKEDITOR.instances.description.getData();
    fd.append('description',desctext);
    fd.append("form", "updblogmodelistic");

    alert(desctext);
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,
        beforeSend: function(){
         $("#loading").show();
       },
       complete: function(){
         $("#loading").hide();
       },   
    })
    .done(function(res) {
        //alert(res);
        if(res==1)
        {
          $('#update-response-message').html('Updated Successfully');
          $( "#update-response-message" ).fadeOut(10000);
        } else {
          $('#update-response-message').html('Something went wrong');
          $( "#update-response-message" ).fadeOut(10000);
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function removeProductPageContent(id){
    var fd = new FormData();
    fd.append('id',id);
    fd.append("form", "productdescrdelete");
    $.ajax({
    url: getBaseURL()+'assets/handler/IotConnecthandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,   
    })
    .done(function(res) {
        //alert(res);
        if(res==1)
        {
            alert("Removed successfully");

          window.location="iot_connectivity";
        } else {
          // $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


function addProcess(id)
{

    var fd = new FormData($("#process")[0]);
    fd.append("form", "addprocess");


     $.ajax({
                url: getBaseURL()+'assets/handler/IotConnecthandler.php',
                type: 'POST',
                //data: $form.serialize(),
                data: fd,
                enctype: 'multipart/form-data',
                processData: false,  
                contentType: false,
                    beforeSend: function(){
                     $("#loading").show();
                   },
                   complete: function(){
                     $("#loading").hide();
                   },   
             })
       .done(function(res) {
       if(res==1)
        {
        
          $('#title').val('');
          $('#description').val('');
           $('#update').val('');
            $('#button').val('add');
            loadProcess();  
            swal("Good job!", "Process Updated Successfully!", "success")
        } else {
          $('#update-response-message').html('Something went wrong');
          $( "#update-response-message" ).fadeOut(10000);
        }
        
    })
     .fail(function() {
         // console.log("error");
     });
}

  function editProcess(id)
  {
    $.ajax({    //create an ajax request to load_page.php
          type: 'POST',
          url: getBaseURL()+'assets/handler/IotConnecthandler.php',             
          dataType: "JSON",   //expect html to be returned   
          data:{form:'getprocess',id:id},            
          success: function(response){                    
      
         
          var records= '';
          var j=1;
          $('#button').val('update');
          document.getElementById("description").value=response[0].description;
          $('#title').val(response[0].title);
          $('#update').val(response[0].id);

        
          
          }

  });
  }

function loadProcess()
{
   $.ajax({    //create an ajax request to load_page.php
        type: 'POST',
        url: getBaseURL()+'assets/handler/IotConnecthandler.php',             
        dataType: "JSON",   //expect html to be returned   
        data:{form:'getprocess'},            
        success: function(response){                    
    
       
        var records= '';
        var j=0;
          for(var i=0;i<response.length;i++){
                var j=j+1;
                records+='<tr><td>'+j+'</td><td>'+response[i].title+'</td><td>'+response[i].description+'</td><td> <button class="btn btn-xs btn-success btn-flat" onclick="editProcess('+response[i].id+')">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button><button class="btn btn-xs btn-warning btn-flat" id="user-inactivate-" onclick="deleteProcess('+response[i].id+')">Delete</button></td></tr>';
             
           }


          $('#content').html(records);
        }

});
}

function deleteProcess(id)
{


  swal({
  title: "Are you sure?",
  text: "User will not be able to see this process!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){

                     $.ajax({    //create an ajax request to load_page.php
              type: 'POST',
              url: getBaseURL()+'assets/handler/IotConnecthandler.php',             
              dataType: "JSON",   //expect html to be returned   
              data:{form:'deleteprocess',id:id},            
              success: function(response)
              {          
              loadProcess();            
                swal("Deleted!", "Process Deleted.", "success");
                 
              }

         });
  
});


     
}

