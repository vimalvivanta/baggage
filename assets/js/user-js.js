
/*Add New User For Admin */
function AddAdminRegister(id) 
{   
  //alert('dhgdbc');
        if ($('#user_name').val() == null || $('#user_name').val()  == "") {
            $('#user_name-error').html("Please enter User name");
            return false;
        } 
        else{
          $('#user_name-error').html("");
        }
      
        var emailReg = /^([A-Za-z0-9._]+@([a-z-]+\.)+[\w-]{2,4})?$/;
        var email=$('#uemail').val();
        if ($('#uemail').val() == "" || !emailReg.test(email)){
             $('#uemail-error').html("Please enter valid email");
            return false;
        } 
        else{
        $('#uemail-error').html("");
        }
        
        if ($('#umobile').val() == null  || $('#umobile').val()  == "") {
            $('#umobile-error').html("Please enter mobile");
            return false;
        } 
        else{
          $('#umobile-error').html("");
        }

          
        var $loadContainer = $("#ajaxLoader-inner"); 
        var $preloadHTML = $("<div class='loader'><img src='"+getBaseURL()+'assets/images/loading.gif'+"' /></div>"); 
       
        var fd = new FormData($("#admin-register-form")[0]);    
        fd.append("form", "AddAdminRegister");              
        $.ajax({
        url: getBaseURL()+'assets/handler/UserHandler.php',
        type: 'POST',
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  
        contentType: false,
        beforeSend: function(){
          $("#add_user").html('Processing...');
          $loadContainer.html($preloadHTML);
        }
        })
        .done(function(res) {
          console.log(res);
            setTimeout(function() {
             $preloadHTML.remove();
            }, 1000);
                         
            if(res==1)
            {   
              document.forms['admin-register-form'].reset();
               $("#add_user").html('Save');
               window.location.href = "all-user-details";
             } 
            else if(res==2)
            {             
              alert("Email Id Already Registerd Please Try Another Email ID");
            }
            else
            {             
              alert("error");
            }
        })
          
}
/* Update User Details */
function updateUserDetails(id) 
{   
       
    var $loadContainer = $("#ajaxLoader-inner"); 
    var $preloadHTML = $("<div class='loader'><img src='"+getBaseURL()+'assets/images/loading.gif'+"' /></div>"); 
   
    var fd = new FormData($("#admin-register-form")[0]);    
    fd.append("form", "updateUserDetails");              
    $.ajax({
    url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,
    beforeSend: function(){
      $("#add_user").html('Processing...');
      $loadContainer.html($preloadHTML);
    }
    })
    .done(function(res) {
      console.log(res);
        setTimeout(function() {
         $preloadHTML.remove();
        }, 1000);
                     
        if(res==1)
              {   
                document.forms['admin-register-form'].reset();
                 $("#add_user").html('Save');
                 window.location.href = "all-user-details";
               } 
              
              else
              {             
                alert("error");
              }
    })
          
}
/*Change User Status*/
function UserStatus(user_id,user_status){
 //alert(user_id);
      if(user_status==0){
        $('#user-activate-'+user_id).removeClass('hide');
        $('#user-inactivate-'+user_id).addClass('hide');
        $('#user_status-'+user_id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+user_id).html('<span style="color:red">Inactive</span>');

      }
      else{
        $('#user-activate-'+user_id).addClass('hide');
        $('#user-inactivate-'+user_id).removeClass('hide');
        $('#user_status-'+user_id).html('<span class="green">Active</span>');
        $('#userstatus-'+user_id).html('<span style="color:green">Active</span>');
      }
      var fd = new FormData();
      fd.append('status', user_status);       // for status
      fd.append('user_id', user_id);
      fd.append("form", "UserStatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/UserHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}
/*User Modal*/
function user_dtl_modal(user_id)
{ 
  //alert('jcbdc');
   $.ajax({
      type: "POST",
      url: getBaseURL()+'assets/includes/user-dtl-modal.php',
      data: {user_id:user_id},
      success: function(data){
        console.log(data);
        $("#user-result").html(data);
       $('#mod-user').modal('show');// this triggers your modal to display
       },

   })
    .fail(function() {
        console.log();
      });

}

/*Check User Name*/
function checkUsername(value){
  //alert('dvbdjv');
  var username = $('#user_name').val();
  var fd = new FormData();
  var id = "check-username";       
  fd.append('username', username);
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
   // return(res);
    console.log(res);
    if($.trim(res)==0){
     $('#user_name-error').html('');
    }
    else{

       $('#user_name-error').html('<span class="red">*</span> This Username is Already Registered!! Try with another');
    }
  })
  .fail(function() {
    console.log("error");
  });
}
/*Check Emails*/
function checkEmail(value){
  //alert('dvbdjv');
  var uemail = $('#uemail').val();
  var fd = new FormData();
  var id = "check-uemail";       
  fd.append('uemail', uemail);
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
   // return(res);
    console.log(res);
    if($.trim(res)==0){
     $('#uemail-error').html('');
    }
    else{
       $('#uemail-error').html('<span class="red">*</span> This Email is Already Registered!! Try with another');
    }
  })
  .fail(function() {
    console.log("error");
  });
}
/*Check Phone*/
function checkPhone(value){
  //alert('dvbdjv');
  var umobile = $('#umobile').val();
  var fd = new FormData();
  var id = "check-umobile";       
  fd.append('umobile', umobile);
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
   // return(res);
    console.log(res);
    if($.trim(res)==0){
     $('#umobile-error').html('');
    }
    else{
       $('#umobile-error').html('<span class="red">*</span> This Mobile is Already Registered!! Try with another');
    }
  })
  .fail(function() {
    console.log("error");
  });
}

/*AjaxCheckUsername*/
function ajaxCheckUsername(username)
{
  var id = "check-username"; 
  var ret;
    // this will generate another thread to run in another function
    jQuery.ajax({
        url: getBaseURL()+'assets/handler/UserHandler.php',
        type: 'POST',
        data: {username:username,form:id},
        async: false,
       success: function(data) {
            ret=data;
            return(data);
            
        }
    });
    // it will always return here in function
      return ret;
}
/*AjaxCheckUsername*/
function ajaxCheckEmail(uemail)
{
  var id = "check-uemail"; 
  var ret;
    // this will generate another thread to run in another function
    jQuery.ajax({
        url: getBaseURL()+'assets/handler/UserHandler.php',
        type: 'POST',
        data: {uemail:uemail,form:id},
        async: false,
       success: function(data) {
            ret=data;
            return(data);
            
        }
    });
    // it will always return here in function
      return ret;
}/*AjaxCheckUsername*/
function ajaxCheckPhone(umobile)
{
  var id = "check-umobile"; 
  var ret;
    // this will generate another thread to run in another function
    jQuery.ajax({
        url: getBaseURL()+'assets/handler/UserHandler.php',
        type: 'POST',
        data: {umobile:umobile,form:id},
        async: false,
       success: function(data) {
            ret=data;
            return(data);
            
        }
    });
    // it will always return here in function
      return ret;
}


/*Send OTP*/
function sendOtp(id) 
{
     
     // alert('hbsdcsh');
     var username=$('#user_name').val();
   
    if($('#user_name').val()=='')
    {
      $('#user_name-error').html('<span class="red">*</span> This field is required');
      return false;
    } 
    else if(ajaxCheckUsername($('#user_name').val())==1)
    {
     //  alert(myFunction(username));
      $('#user_name-error').html('<span class="red">*</span> This Username is Already Registered!! Try with another');
      return false;
    } 
    else 
    {
     
      $('#user_name-error').html('');

    }
      var emailReg = /^([A-Za-z0-9._]+@([a-z-]+\.)+[\w-]{2,4})?$/;
      var email=$('#uemail').val();
    if($('#uemail').val()=='' || !emailReg.test(email))
    {
      $('#uemail-error').html('<span class="red">*</span> Please enter valid email');
      return false;
    }
    else if(ajaxCheckEmail($('#uemail').val())==1)
    {
     //  alert(myFunction(username));
     $('#uemail-error').html('<span class="red">*</span> This Email is Already Registered!! Try with another');
      return false;
    } 
    else
    {
      $('#uemail-error').html('');

    }
    if($('#passwords').val()=='')
    {
      $('#error-passwords').html('<span class="red">*</span> This field is required');
      return false;
    }
    else
    {
      $('#error-passwords').html('');
    }
    if($('#umobile').val()=='')
    {
      $('#umobile-error').html('<span class="red">*</span> This field is required');
      return false;
    }
     else if(ajaxCheckPhone($('#umobile').val())==1)
    {
     //  alert(myFunction(username));
      $('#umobile-error').html('<span class="red">*</span> This Mobile is Already Registered!! Try with another');
      return false;
    } 
    else
    {
      $('#umobile-error').html('');
    }
    
   
     
    var fd = new FormData();
           
    fd.append('username', $('#user_name').val());
    fd.append('password', $('#passwords').val());
    fd.append('umobile', $('#umobile').val());
    fd.append('uemail', $('#uemail').val());
    fd.append("form", id); 

    $.ajax({
       url: getBaseURL()+'assets/handler/UserHandler.php',
      type: 'POST',   
      data: fd,
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      beforeSend:function(){
        $('#send-otp').text('Processing...');
       
        
      }
    })
    .done(function(res) {
        console.log(res);
        // alert(res);
        // return;
    if(res){
      $("#reg-user").hide();
      $("#otp-popup").show();
      document.getElementById("hidotp").value = res;
      
    }else{ 
     $('#send-otp').text('Sign Up');
    }
    })
    .fail(function() {
    alert('error');
    });
 
}

/*User Register*/
function userRegister(id)
{

    if($('#full_name').val()=='')
    {
      $('#full_name-error').html('<span class="red">*</span> This field is required');
      return false;
    }
    else 
    {
     
      $('#user_name-error').html('');

    }
      var emailReg = /^([A-Za-z0-9._]+@([a-z-]+\.)+[\w-]{2,4})?$/;
      var email=$('#uemail').val();
    if($('#uemail').val()=='' || !emailReg.test(email))
    {
      $('#uemail-error').html('<span class="red">*</span> Please enter valid email');
      return false;
    }
    else if(ajaxCheckEmail($('#uemail').val())==1)
    {
     //  alert(myFunction(username));
     $('#uemail-error').html('<span class="red">*</span> This Email is Already Registered!! Try with another');
      return false;
    } 
    else
    {
      $('#uemail-error').html('');

    }
    if($('#passwords').val()=='')
    {
      $('#error-passwords').html('<span class="red">*</span> This field is required');
      return false;
    }
    else
    {
      $('#error-passwords').html('');
    }

    if($('#umobile').val()=='')
    {
      $('#umobile-error').html('<span class="red">*</span> This field is required');
      return false;
    }
     else if(ajaxCheckPhone($('#umobile').val())==1)
    {
     //  alert(myFunction(username));
      $('#umobile-error').html('<span class="red">*</span> This Mobile is Already Registered!! Try with another');
      return false;
    } 
    else
    {
      $('#umobile-error').html('');
    }


  var fd = new FormData($("#register-form")[0]);
  fd.append("form", id);
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    beforeSend:function(){
      $('#user-register').text('Saving...');
    }
  })
  .done(function(res) {
  console.log(res);
  //alert(res);
  if(res==1){
  $('#successfull').html('Registered Successfull');
   window.location.href = "user-dashboard";
  }
  else{
   $('#mobile_otp-error').html('<span class="red">*</span> Registration failed');
 }
  })
  .fail(function() {
  console.log("error");
  });
}
/*Login*/
function userLogin(id)
{
  // alert(0);
  if($('#login_Email').val()=='')
  {
    $('#error-login_Email').html('This field is required');
    return false;
  }
  else
  {
    $('#error-login_Email').html('');
  }

  if($('#login_Password').val()=='')
  {
    $('#error-login_Password').html('This field is required');
    return false;
  }
  else
  {
    $('#error-login_Password').html('');
  }


  var fd = new FormData();
  fd.append('email', $('#login_Email').val());        
  fd.append('password', $('#login_Password').val());         
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
     //alert(res);
    if(res==0){
      $('#login_error').html('Invalid Email Or Password!!');
    }
    else{
      window.location.href = "user-dashboard";
    }

  })
  .fail(function() {
  //console.log("error");
  });

}

/*Forgot Password*/
function forgotPassword(id)
{
  // alert(0);
  $('#result_forgetpassword').html('');
  $('#error-forgotpassword').html(''); 

  if($('#forgot_email').val()=='')
  {
    $('#error-forgot_email').html('This field is required');
    return false;
  }
  else
  {
    $('#error-forgot_email').html('');
  }

  var fd = new FormData();
  fd.append('forgot_email', $('#forgot_email').val());
  fd.append("form", id); 
    $.ajax({
       url: getBaseURL()+'assets/handler/UserHandler.php',
      type: 'POST',   
      data: fd,
      enctype: 'multipart/form-data',
      processData: false, 
      contentType: false,

    })
    .done(function(res) {
      
      if(res==1){
        $('#result_forgetpassword').html('An Email Has been sent Please Check Your Email'); 
      }

      else if(res==0){
        $('#error-forgotpassword').html('Invalid Email Address'); 
      }

    })
    .fail(function() {
  //console.log("error");
  });

}
/*Reset Password*/
function resetPassword(id){
// alert(0);


  if($('#reset_password').val()=='')
  {
    $('#error-reset_password').html('This field is required');
    return false;
  }
  else
  {
    $('#error-reset_password').html('');
  }

if($('#reset_cpassword').val()=='')
  {
    $('#error-reset_cpassword').html('This field is required');
    return false;
  }
  else
  {
    $('#error-reset_cpassword').html('');
  }

    if($('#reset_password').val()!=$('#reset_cpassword').val()){
    $('#error-reset_cpassword').html('Password not matching');
    return false;

  }
// alert(user_id);

  var fd = new FormData();
  fd.append('new_password', $('#reset_password').val());
  fd.append('user_id', $('#user_id').val());
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false, 
    contentType: false,

  })
  .done(function(res) {
    
    if(res==1){
     $('#result_resetpassword').html('Changes Saved Successfully');
    }

  })
  .fail(function() {
//console.log("error");
});

}
function ChangeEmail(id){
// alert(0);
  $('#result_email').html(' ');



  var emailReg = /^([A-Za-z0-9._]+@([a-z-]+\.)+[\w-]{2,4})?$/;
  var email=$('#c_email').val();
  if($('#c_email').val()=='' || !emailReg.test(email))
  {
    $('#error-c_email').html('<span class="red">*</span> Please enter valid email');
    return false;
  }
  else
  {
    $('#error-c_email').html('');
  }


  
  var fd = new FormData();
  fd.append('c_email', $('#c_email').val()); 
  fd.append('user_id', $('#user_id').val());          
  fd.append("form", id); 
  $.ajax({
     url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
    console.log(res);
    if(res==1){
      $('#error-c_email').html('Email Already Registered Try with other');
    } 
    else{

      $('#result_email').html('Email Changed Successfully');

    }
  })
  .fail(function() {
//console.log("error");
});
} 
function ChangeMob(id){

  $('#result_phone').html(' ');
  
  if($('#c_phone').val()=='')
  {
    $('#error-c_phone').html('This field is required');
    return false;
  }
  else
  {
    $('#error-c_phone').html('');
  }

  var fd = new FormData();
          
  fd.append('c_phone', $('#c_phone').val());
  fd.append('user_id', $('#user_id').val());          
  fd.append("form", id); 
  $.ajax({
    url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
    console.log(res);
    if(res==1){
      $('#error-c_phone').html('Phone Number Already Registered Try with other');
    } 
    else{

      $('#result_phone').html('Phone Number Changed Successfully');

    }
  })
  .fail(function() {
//console.log("error");
});
} 
function ChangeUser(id){

  $('#result_name').html(' ');
  
  if($('#u_name').val()=='')
  {
    $('#error-u_name').html('This field is required');
    return false;
  }
  else
  {
    $('#error-u_name').html('');
  }

  var fd = new FormData();
          
  fd.append('u_name', $('#u_name').val());
  fd.append('user_id', $('#user_id').val());          
  fd.append("form", id); 
  $.ajax({
    url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
    console.log(res);
    if(res==1){
      $('#error-u_name').html('User Name Already Registered Try with other');
    } 
    else{

      $('#result_name').html('User Name Changed Successfully');

    }
  })
  .fail(function() {
//console.log("error");
});
} 

/* Update User Details */
function updateUsersDetails(id) 
{   
  // alert(0);
       
    var $loadContainer = $("#ajaxLoader-inner"); 
    // var $preloadHTML = $("<div class='loader'><img src='"+getBaseURL()+'assets/images/loading.gif'+"' /></div>"); 
   
    var fd = new FormData($("#admin-register-form")[0]);    
    fd.append("form", "add_user_details");              
    $.ajax({
      url: getBaseURL()+'assets/handler/UserHandler.php',
    type: 'POST',
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,  
    contentType: false,
    beforeSend: function(){
      $("#add_user").html('Processing...');
      // $loadContainer.html($preloadHTML);
    }
    })
    .done(function(res) {
      console.log(res);
        // setTimeout(function() {
        //  $preloadHTML.remove();
        // }, 1000);
                     
        if(res==1)
              {   
                
                 window.location.href = "";
               } 
              
              else
              {             
                alert("error");
              }
    })
          
}