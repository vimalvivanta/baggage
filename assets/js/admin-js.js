/*--------- Admin login sec -------------*/
function execLogin() 
{  
         $.ajax({
            url: getBaseURL()+'assets/controller/AdminLoginController.php',
            type: 'POST',       
            data: $("#adminlog-form").serialize(),
        })
        .done(function(res) {
             if(res==1){
                window.location.href = "dashboard";
            } 
            else if(res==0)
            {
            $('#alertContainer')
                .removeClass('alert-danger')
                .addClass('alert-danger')
                .html('Invalid Credentials..!! Try Again')
                .show();
              
            }
          
            else
            {
            	alert("error");
            }
        })
        .fail(function() {
            //console.log("error");
        });


}

/*Password change*/
function ExecChangePsd(id) 
{   

    if ($('#oldpassword').val() == null || $('#oldpassword').val()  == "") {
        $('#old-psd-error').html("Please enter oldpassword");
        return false;
    } 
    else{
      $('#old-psd-error').html("");
    }
    if ($('#newpassword').val() == null || $('#newpassword').val()  == "") {
        $('#new-psd-error').html("Please enter new password");
        return false;
    } 
    else{
      $('#new-psd-error').html("");
    }
    if ($('#confirmpassword').val() == null || $('#confirmpassword').val()  != $('#newpassword').val() || $('#confirmpassword').val()  == "") {
        $('#confirm-psd-error').html("Please enter confirm password");
        return false;
    } 
    else{
      $('#confirm-psd-error').html("");
    }
    
    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
     data: $("#setting-form").serialize()+"&form="+id,
   
    })
    .done(function(res) {
        console.log(res);
        if(res==1)
        {
            //window.location.href = "dashboard.php";
            document.forms['setting-form'].reset();
            
            $("#alertContainer").show();   
             
            $('#alertContainer').delay(5000).fadeOut('fast', function(){
            $('#alertContainer').hide(); 
            });
        }
        else
        {
            document.forms['setting-form'].reset();
                
            alert("Enter Correct password");
        }
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Add Admin By Super Admin*/
function addAdmin(id) 
{   

    if($('#admin_name').val() ==""){
        $('#error_admin_name').html("Please enter the admin name.");
        return false;
     }else{
        $('#error_admin_name').html("");
     }
    if($('#admin_email').val() ==""){
        $('#error_admin_email').html("Please enter the admin email.");
        return false;
     }else{
        $('#error_admin_email').html("");
     }
    if($('#admin_pass').val() ==""){
        $('#error_admin_pass').html("Please enter the password.");
        return false;
     }else{
        $('#error_admin_pass').html("");
     }
    var $loadContainer = $("#ajaxLoader-inner"); 
    var $preloadHTML = $("<div class='loader'><img src='"+getBaseURL()+'assets/images/loading.gif'+"' /></div>"); 
    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: $("#addadmin-form").serialize()+"&form="+id,
   
    })
    .done(function(res) {
        
        if(res==1)
        {
            window.location.href = "view-admin";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Update Admin By Super Admin*/
function updateAdminDetails(id) 
{   

    if($('#admin_name').val() ==""){
        $('#error_admin_name').html("Please enter the admin name.");
        return false;
     }else{
        $('#error_admin_name').html("");
     }
    if($('#admin_email').val() ==""){
        $('#error_admin_email').html("Please enter the admin email.");
        return false;
     }else{
        $('#error_admin_email').html("");
     }
    if($('#admin_pass').val() ==""){
        $('#error_admin_pass').html("Please enter the password.");
        return false;
     }else{
        $('#error_admin_pass').html("");
     }    


    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: $("#addadmin-form").serialize()+"&form="+id,
   
    })
    .done(function(res) {
        
        if(res==1)
        {
            window.location.href = "view-admin";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Change Admin Status*/
function AdminStatus(admin_id,admin_status)
{

    if(admin_status==0)
    {
        $('#admin-activate-'+admin_id).removeClass('hide');
        $('#admin-inactivate-'+admin_id).addClass('hide');
        $('#admin_status-'+admin_id).html('<span class="red">Inactive</span>');
        $('#adminstatus-'+admin_id).html('<span style="color:red">Inactive</span>');

    }else{
        $('#admin-activate-'+admin_id).addClass('hide');
        $('#admin-inactivate-'+admin_id).removeClass('hide');
        $('#admin_status-'+admin_id).html('<span class="green">Active</span>');
        $('#adminstatus-'+admin_id).html('<span style="color:green">Active</span>');
    }
      var fd = new FormData();
      fd.append('status', admin_status);       // for status
      fd.append('admin_id', admin_id);
      fd.append("form", "change-admin-status");
      $.ajax({
        url: getBaseURL()+'assets/handler/AdminHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}
function checkUsername(value){
  //alert('dvbdjv');
  var username = $('#admin_email').val();
  var fd = new FormData();
  var id = "check-admin-username";       
  fd.append('username', username);
  fd.append("form", id); 
  $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',   
    data: fd,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,

  })
  .done(function(res) {
   // return(res);
    console.log(res);
    if($.trim(res)==1){
      $('#error_admin_email').html('<span class="red">*</span> This Username is Already Registered!! Try with another');
    }
    else{
      $('#error_admin_email').html('');
    }
  })
  .fail(function() {
    console.log("error");
  });
}

/*Update Social Links*/
function updateSocialLinks(id) 
{   
    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: $("#create-sociallink-form").serialize()+"&form=sociallinks",
   
    })
    .done(function(res) {
        
        if(res==1)
        {
            
          $('#update-response-message').html('Updated Successfully');
            
        } else {
          $('#update-response-message').html('Something went wrong');
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function addBanner(id) 
{   

    if($('#page_name').val() ==""){
        $('#error_page_name').html("Please choose page name.");
        return false;
     }else{
        $('#error_page_name').html("");
     }

             if($('#page_name').val() =="Home page Top")
        {
                if($('#priority').val() ==""){
                alert('kindly enter the priority');
                  return false;
               }   
        }

    var fd = new FormData($("#create-banner-form")[0]);
    fd.append("form", id);
    //alert(fd);
    // var event_content=CKEDITOR.instances.blog-text.getData();
    // fd.append('event_content',event_content);

    $.ajax({
      url: getBaseURL()+'assets/handler/AdminHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
      beforeSend: function(){
         $("#loading").show();
      },
      complete: function(){
         $("#loading").hide();
      }, 
    })
    .done(function(res) {
        //console.log(res);
        //alert(res);
        if(res==1)
        {
            window.location.href = "banners-list";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

/*Change banner Status*/
function bannerStatus(id,status) {
 // alert(id);
      if(status==0) {
        $('#user-activate-'+id).removeClass('hide');
        $('#user-inactivate-'+id).addClass('hide');
        $('#user_status-'+id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+id).html('<span style="color:red">Inactive</span>');
      }
      else{
        $('#user-activate-'+id).addClass('hide');
        $('#user-inactivate-'+id).removeClass('hide');
        $('#user_status-'+id).html('<span class="green">Active</span>');
        $('#userstatus-'+id).html('<span style="color:green">Active</span>');
      }

      var fd = new FormData();
      fd.append('status', status);       // for status
      fd.append('id', id);
      fd.append("form", "bannerStatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/AdminHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        //alert(res);
        //console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}

function updateStore(id) 
{   


    var fd = new FormData($("#create-store-form")[0]);
    fd.append("form", id);
    //alert(fd);
    // var event_content=CKEDITOR.instances.blog-text.getData();
    // fd.append('event_content',event_content);

    $.ajax({
      url: getBaseURL()+'assets/handler/AdminHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
    })
    .done(function(res) {
        //console.log(res);
        //alert(res);
        if(res==1)
        {
            $('#update-response-message').html('Updated Successfully'); 
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}

function updateMyprofile(id) 
{   
    var fd = new FormData($("#create-profile-form")[0]);
    fd.append("form", id);
    //alert(fd);
    // var event_content=CKEDITOR.instances.blog-text.getData();
    // fd.append('event_content',event_content);

    $.ajax({
      url: getBaseURL()+'assets/handler/AdminHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
    })
    .done(function(res) {
        //console.log(res);
        //alert(res);
        if(res==1)
        {
            $('#update-response-message').html('Updated Successfully'); 
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}