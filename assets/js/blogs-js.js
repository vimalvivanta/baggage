
/*Add Admin By Super Admin*/
function addblog(id) 
{   

    if($('#blog-title').val() ==""){
        $('#error-blog-title').html("Please enter the blog title.");
        return false;
     }else{
        $('#error-blog-title').html("");
     }

    if($('#blog-text').val() ==""){
        $('#error-blog-text').html("Please enter the blog text.");
        return false;
     }else{
        $('#error-blog-text').html("");
     }

      var blogimage=$('#blogimage').val();
      

    // var fd = new FormData();    
    // fd.append("form", id);
    // fd.append('title',$('#blog-title').val());
    // fd.append('text',$('#blog-text').val());
    // fd.append('blogimage',blogimage);
    var fd = new FormData($("#create-blog-form")[0]);
    fd.append("form", 'blog-upload-btn');
    //alert(fd);
    var blog_text=CKEDITOR.instances.blog_text.getData();
    fd.append('blogtext',blog_text);

    $.ajax({
      url: getBaseURL()+'assets/handler/BlogsHandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
        beforeSend: function(){
         $("#loading").show();
       },
       complete: function(){
         $("#loading").hide();
       },      
    })
    .done(function(res) {
        //console.log(res);
        //alert(res);
        if(res==1)
        {
            window.location.href = "blogs-list";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


/*Change blog Status*/
function blogStatus(id,status) {
 //alert(user_id);
      if(status==0) {
        $('#user-activate-'+id).removeClass('hide');
        $('#user-inactivate-'+id).addClass('hide');
        $('#user_status-'+id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+id).html('<span style="color:red">Inactive</span>');
      }
      else{
        $('#user-activate-'+id).addClass('hide');
        $('#user-inactivate-'+id).removeClass('hide');
        $('#user_status-'+id).html('<span class="green">Active</span>');
        $('#userstatus-'+id).html('<span style="color:green">Active</span>');
      }

      var fd = new FormData();
      fd.append('status', status);       // for status
      fd.append('id', id);
      fd.append("form", "BlogStatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/BlogsHandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        //alert(res);
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}


/*
function add_events(id) { 
    //tinyMCE.triggerSave();   
    var event_title=$('#event_title').val();
    var event_cnt_heading=$('#event_cnt_heading').val();
    var event_content=CKEDITOR.instances.event_content.getData();
    
    var event_image=$('#event_image').val();
    //var event_upfile=$('#event_upfile').val();    
    var messageLength = CKEDITOR.instances['event_content'].getData().replace(/<[^>]*>/gi, '').length;
    
    if(event_title ==''){      
      $('.error_title').html("Please Enter Event Title");
      return false;
    }else{
      $('.error_title').html();
    }
    if(event_cnt_heading ==''){      
      $('.error_head').html("Please Enter Event Content Heading");
      return false;
    }else{
      $('.error_head').html();
    }
    if( !messageLength ) {
      $('.error_content').html("Please Enter Event Content");
      return false;
    }else{
      $('.error_content').html();
    }    
    if(event_image ==''){      
    $('.error_img').html("Please Upload Event Image");
    return false;
  }else{
    $('.error_img').html();
  }
  if(!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(event_image)) {
        $('.error_img').html("Please Upload Image only");
        return false;
  }
  else{
    $('.error_img').html("");
  }
  // if(event_upfile ==''){      
  //   $('.error_file').html("Please Upload Event File");
  //   return false;
  // }else{
  //   $('.error_file').html();
  // }
  // if(!(/\.(pdf|doc|docx|xls|xlsx)$/i).test(event_upfile)) {
  //       $('.error_file').html("Please Upload Files only");
  //       return false;
  // }
  // else{
  //   $('.error_file').html("");
  // }
    var fd = new FormData($("#adminevt-form")[0]);    
    fd.append("form", id);
    fd.append('event_content',event_content);
    $.ajax({
      url: getBaseURL()+'assets/handler/EventsHandler.php',
      type: 'POST',       
      //data: $("#adminvert-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
      beforeSend: function(){
          $('#fadebx').show();
          $('#loaderImg').show();          
        }
    })
    .done(function(res) {
        //alert(res);                
        if(res==0){
          
            $('#fadebx').hide();
            $('#loaderImg').hide();
            $(".news-error").show();
            $(".news-error").html("Invalid..!! Try Again");
            $(".news-error").delay(5000).fadeOut('slow');
        }         
        else if(res == 1) {                     
            window.location.href = "view-events.php?msg=Success";
        }
    })
    .fail(function() {
        // console.log("error");
    });
}
*/

/*Update Admin By Super Admin*/
function updateAdminDetails(id) 
{   

    if($('#admin_name').val() ==""){
        $('#error_admin_name').html("Please enter the admin name.");
        return false;
     }else{
        $('#error_admin_name').html("");
     }
    if($('#admin_email').val() ==""){
        $('#error_admin_email').html("Please enter the admin email.");
        return false;
     }else{
        $('#error_admin_email').html("");
     }
    if($('#admin_pass').val() ==""){
        $('#error_admin_pass').html("Please enter the password.");
        return false;
     }else{
        $('#error_admin_pass').html("");
     }    


    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
    data: $("#addadmin-form").serialize()+"&form="+id,
   
    })
    .done(function(res) {
        
        if(res==1)
        {
            window.location.href = "view-admin";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


function deleteBlog(id)

{




    swal({
  title: "Are you sure?",
  text: "User will not be able to see this blog!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){

                     $.ajax({    //create an ajax request to load_page.php
              type: 'POST',
              url: getBaseURL()+'assets/handler/BlogsHandler.php',             
              dataType: "JSON",   //expect html to be returned   
              data:{form:'deleteblogs',id:id},            
              success: function(response)
              {          
               
                swal("Deleted!", "Blog Deleted.", "success");

                window.location.reload();
                 
              }

         });
  
});

}


function deleteBanners(id)

{




    swal({
  title: "Are you sure?",
  text: "User will not be able to see this banner!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){

                     $.ajax({    //create an ajax request to load_page.php
              type: 'POST',
              url: getBaseURL()+'assets/handler/BlogsHandler.php',             
              dataType: "JSON",   //expect html to be returned   
              data:{form:'deletebanners',id:id},            
              success: function(response)
              {          
               
                swal("Deleted!", "Banner Deleted.", "success");

                window.location.reload();
                 
              }

         });
  
});

}