
/*Add Admin By Super Admin*/
function addblog(id) 
{   

    if($('#blog-title').val() ==""){
        $('#error-blog-title').html("Please enter the blog title.");
        return false;
     }else{
        $('#error-blog-title').html("");
     }

    

      var blogimage=$('#blogimage').val();

  
      
      

    // var fd = new FormData();     
    // fd.append("form", id);
    // fd.append('title',$('#blog-title').val());
    // fd.append('text',$('#blog-text').val());
    // fd.append('blogimage',blogimage);
    var fd = new FormData($("#create-blog-form")[0]);
    fd.append("form", 'blog-upload-btn');
    //alert(fd);

      
    var event_content=CKEDITOR.instances.blog_text.getData();
    fd.append('blogtext',event_content);


   if(event_content==""){
        $('#error-blog-text').html("Please enter the blog text.");
        return false;
     }else{
        $('#error-blog-text').html("");
     }



    $.ajax({
      url: getBaseURL()+'assets/handler/Iotapphandler.php',
      type: 'POST',
      //data: $("#create-blog-form").serialize()+"&form="+id,
      data:fd,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false,
    })
    .done(function(res) {
        console.log(res);
        //alert(res);
        if(res==1)
        {
            window.location.href = "iot_applications";
            
        }
        
    })
     .fail(function() {
         // console.log("error");
     });

}


/*Change blog Status*/
function blogStatus(id,status) {
 //alert(user_id);
      if(status==0) {
        $('#user-activate-'+id).removeClass('hide');
        $('#user-inactivate-'+id).addClass('hide');
        $('#user_status-'+id).html('<span class="red">Inactive</span>');
        $('#userstatus-'+id).html('<span style="color:red">Inactive</span>');
      }
      else{
        $('#user-activate-'+id).addClass('hide');
        $('#user-inactivate-'+id).removeClass('hide');
        $('#user_status-'+id).html('<span class="green">Active</span>');
        $('#userstatus-'+id).html('<span style="color:green">Active</span>');
      }

      var fd = new FormData();
      fd.append('status', status);       // for status
      fd.append('id', id);
      fd.append("form", "BlogStatus");
      $.ajax({
        url: getBaseURL()+'assets/handler/Iotapphandler.php',
        type: 'POST',   
        data: fd,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
      })
      .done(function(res) {
        //alert(res);
        console.log(res);
        if(res==1){
         
        } else {
          alert('error');
        }
      })
      .fail(function() {
        //console.log("error");
      });
}


function deleteBlog(id)

{




    swal({
  title: "Are you sure?",
  text: "User will not be able to see this blog!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){

                     $.ajax({    //create an ajax request to load_page.php
              type: 'POST',
              url: getBaseURL()+'assets/handler/Iotapphandler.php',             
              dataType: "JSON",   //expect html to be returned   
              data:{form:'deletecase',id:id},            
              success: function(response)
              {          
               
                swal("Deleted!", "Blog Deleted.", "success");

                window.location.reload();
                 
              }

         });
  
});

}