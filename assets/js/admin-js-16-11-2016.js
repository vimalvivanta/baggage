/*--------- Admin login sec -------------*/
function execLogin() 
{  
         $.ajax({
            url: getBaseURL()+'assets/controller/AdminLoginController.php',
            type: 'POST',       
            data: $("#adminlog-form").serialize(),
        })
        .done(function(res) {
             if(res==1){
                window.location.href = "dashboard";
            } 
            else if(res==0)
            {
            $('#alertContainer')
                .removeClass('alert-danger')
                .addClass('alert-danger')
                .html('Invalid Credentials..!! Try Again')
                .show();
              
            }
          
            else
            {
            	alert("error");
            }
        })
        .fail(function() {
            //console.log("error");
        });


}

/*Password change*/
function ExecChangePsd(id) 
{   

    if ($('#oldpassword').val() == null || $('#oldpassword').val()  == "") {
        $('#old-psd-error').html("Please enter oldpassword");
        return false;
    } 
    else{
      $('#old-psd-error').html("");
    }
    if ($('#newpassword').val() == null || $('#newpassword').val()  == "") {
        $('#new-psd-error').html("Please enter new password");
        return false;
    } 
    else{
      $('#new-psd-error').html("");
    }
    if ($('#confirmpassword').val() == null || $('#confirmpassword').val()  != $('#newpassword').val() || $('#confirmpassword').val()  == "") {
        $('#confirm-psd-error').html("Please enter confirm password");
        return false;
    } 
    else{
      $('#confirm-psd-error').html("");
    }
    
    $.ajax({
    url: getBaseURL()+'assets/handler/AdminHandler.php',
    type: 'POST',
    //data: $form.serialize(),
     data: $("#setting-form").serialize()+"&form="+id,
   
    })
    .done(function(res) {
        
        if(res==1)
        {
            //window.location.href = "dashboard.php";
            document.forms['setting-form'].reset();
            
            $("#alertContainer").show();   
             
            $('#alertContainer').delay(5000).fadeOut('fast', function(){
            $('#alertContainer').hide(); 
            });
        }
        else
        {
            document.forms['setting-form'].reset();
                
            alert("Enter Correct password");
        }
    })
     .fail(function() {
         // console.log("error");
     });

}