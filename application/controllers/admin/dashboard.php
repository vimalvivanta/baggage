<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

  public function __construct()
  {

    parent::__construct();
  }
 
  public function index()
  {


      if($this->session->userdata('loggedUser')){

      //$data['title']='Home';
      $data['content']='admin_dashboard';
      $this->load->view('admin/base',$data);
    }else{

      redirect(''.base_url().'admin/dashboard/login');
    }

    
  }
  public function login()

  {

    $data['content']='admin_login';
    $this->load->view('admin/base',$data);
  }

  public function validateLogin()
  {

      $admin_model=$this->adminModel->checkLogin($_POST);

        // echo json_encode($admin_model);
        // exit;
      if($admin_model)
      {
        foreach ($admin_model as $row) 
        {
          $userdetails = array(
          "email"=>$row['employee_email'],
          "admin_name"=>$row['employee_name'],
          "admin_id"=>$row['employee_id']
          );

        }
          $this->session->set_userdata("loggedUser",$userdetails);
            if($this->session->userdata('loggedUser')){
            echo json_encode(array("error"=>false,$msg="Login successfully")); 
          }
        
      }
      else
      {
         echo json_encode(array("error"=>true,$msg="Invalid Login Credentials")); 
      }
  }

  public function addlocation()
  {
      if($this->session->userdata('loggedUser')){
         $data['content']='add_location';
         $this->load->view('admin/base',$data);
            
          }
          else
          {
            redirect(''.base_url().'/admin/dashboard');
          }
  }

  public function insertlocation()
  {
      if($this->session->userdata('loggedUser')){

          $data=$_POST;

          $admin_model=$this->adminModel->add_location($data);

           if($admin_model){
            echo json_encode(array("error"=>false,$msg="Location added successfully")); 
          }
          else
          {
             echo  json_encode(array("error"=>true,$msg="Error in adding location")); 
          }
            
          }
          else
          {
            redirect(''.base_url().'/admin/login');
          }
  }

  public function locations()
  {
        if($this->session->userdata('loggedUser')){

             $data['content']='locations';

          
             $this->load->view('admin/base',$data);

    }else{
       redirect(''.base_url().'/admin/login');
    }
  }

  public function getlocations()
  {

          $results=$this->adminModel->getlocation();

          echo json_encode($results);
  }

  public function deletelocation()
  {

      $id=$_POST;
    $deletelocation=$this->adminModel->deletelocation($id);

    echo json_encode($deletelocation);
    exit;
  }

  public function employees()
  {
     if($this->session->userdata('loggedUser')){

             $data['content']='employees';

             $data['employee_list']=$this->getemployee();

          
             $this->load->view('admin/base',$data);

    }else{
       redirect(''.base_url().'/admin/login');
    }
  }

   public function getemployee()
  {

          $results=$this->adminModel->getemployee();

          return $results;
  }

  public function addemployee()
  {


         if($this->session->userdata('loggedUser')){

            $employee_id=base64_decode($_GET['id']);

             $data['content']='add_employee';
             if($employee_id!='')
             {
              $data['employee_id']=$employee_id;
               $data['employee_details']=$this->adminModel->getemployee($employee_id);

               // print_r($data['employee_details']);
               // exit;
             }

          
             $this->load->view('admin/base',$data);

    }else{
       redirect(''.base_url().'/admin/login');
    } 
  }
  public function insertemployee()
  {
    $data=$_POST;
    $added=$this->adminModel->addemployee($data);

    echo json_encode($added);
    exit;
  }

  public function deleteemployee()
  {
    $data=$_POST;

    $deleted=$this->adminModel->deleteemployee($data['employee_id']);

    echo json_encode($deleted);
    exit; 
  }

  public function site_settings()

  {
         if($this->session->userdata('loggedUser')){

        $data['content']='site_settings';
        $data['site_settings']=$this->adminModel->site_settings();

        // print_r($data['site_settings']);
        // exit;
         $this->load->view('admin/base',$data);
          }else{
               redirect(''.base_url().'/admin/login');
            } 
  }

  public function update_settings()
  {
       $data=$_POST;
       $results=$this->adminModel->update_settings($data);
       echo json_encode($results);
       exit;
  }

  public function orders()
  {

      
    
      
       $data['orders']=$this->adminModel->orders();
       $data['content']='orders';
       
       $this->load->view('admin/base',$data);
       return $results;
  }

  public function orderdetails()
  {
      $order_id=$this->uri->segment(4);
       
      $data['orders']=$this->adminModel->orders($order_id);
      $data['content']='order_details';
      // print_r($data['orders']);
      // exit;
        $this->load->view('admin/base',$data);
       return $results;
       print_r($data['orders']);
       exit;
  }



  public function logout()
  {
    $this->session->unset_userdata('loggedUser');
    redirect(''.base_url().'/admin/dashboard');
  }



}/**
*
* Author: CodexWorld
* Function Name: getDistance()
* $addressFrom => From address.
* $addressTo => To address.
* $unit => Unit type.
*
**/
