<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">
          <h1>
           Add Location
            
          </h1>

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">locations</a></li>
            <li class="active">Add Location</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">

            
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- <div class="box-header with-border">
                  <h3 class="box-title">Quick Example</h3>
                </div> --><!-- /.box-header -->
                <!-- form start -->
                <form id="add-location-form" role="form" name="add-location-form" action="#" method="POST" >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="location">Location</label>
                      <input type="text" class="form-control" id="location" required="" name="location" placeholder="Enter the Origin" value="">
                      <span class="error" id="error-location"></span>
                      <input type="hidden" class="form-control" id="job-id" name="job-id" value="<?php echo $jobid; ?>">

                    </div>

                     <!--   <div class="form-group">
                      <label for="Hub">Central Hub</label>
                      <input type="text" class="form-control" id="hub" required="" name="hub" placeholder="Enter the Central Hub" value="">
                      <span class="error" id="error-hub"></span>
                      <input type="hidden" class="form-control" id="hub" name="job-id" value="<?php echo $jobid; ?>">

                    </div>

                       <div class="form-group">
                      <label for="Destination">Destination</label>
                      <input type="text" class="form-control" id="Destination" required="" name="Destination" placeholder="Enter the Destination" value="">
                      <span class="error" id="error-job-location"></span>
                      <input type="hidden" class="form-control" id="job-id" name="job-id" value="<?php echo $jobid; ?>">

                    </div>


                      
                    <div class="form-group">
                      <label for="Hub">Origin to Hub</label>
                      <input type="number" class="form-control" id="location_to_hub" name="location_to_hub" required="" placeholder="Enter distance from Origin to Hub" value="">
                      <span class="error" id="location_to_hub"></span>
                      <input type="hidden" class="form-control" id="hub" name="job-id" value="<?php echo $jobid; ?>">

                    </div>
                     <div class="form-group">
                      <label for="Hub">Hub to Destination</label>
                      <input type="number" class="form-control" id="hub_to_destination" name="hub_to_destination" required="" placeholder="Enter the distance from Hub to Destination" value="">
                      <span class="error" id="hub_to_destination"></span>
                      <input type="hidden" class="form-control" id="hub" name="job-id" value="<?php echo $jobid; ?>">

                    </div>
 -->
                  
<!--                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7Glzdi235cx0H8hLXQESse38ASl8lfx4&callback=initMap"
  type="text/javascript"></script>
 -->                  
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <input  id="job-upload-btn" name="job-upload-btn" type="submit" class="btn btn-primary" >
                   <!--  <button id="job-update-btn" name="job-update-btn" type="button" class="btn btn-primary" onclick="addjob(this.id)">Update</button> -->
                <!--   <span id="loading"><img src="<?php echo IMAGES; ?>loading.gif"></span> -->
                    <div id="update-response-message"></div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>
  $(function () {
swal("Here's a message!");
var base_url ='<?php echo base_url();?>';



$('#add-location-form').submit(function(e){



if($('#location').val() == ''){
  alert('Please enter the Origin');
return false;
}else if($('#hub').val() == ''){
  alert('Please enter the hub');
  return false;
}
else if($('#Destination').val() == ''){
  alert('Please enter the Destination');
  return false;
}
else if($('#location_to_hub').val() == ''){
  alert('Please enter the Distance from Origin to Hub');
  return false;
}
else if($('#hub_to_destination').val() == ''){
  alert('Please enter the Distance from Hub to Destination');
  return false;
}
e.preventDefault();



data = $(this).serialize();
alert(data);

$.ajax({
  url: base_url+'admin/dashboard/insertlocation',
  type: 'POST',
  dataType: 'JSON',
  data: data,
})
.done(function(data) {


  if(data.error == false){

         swal({
           title: "success",
           text: data.msg,
           type: "success",
           showOkButton: false,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/locations';
          });
              
    


  }else{

    alert('Invalid Login Credentials');
      window.location.href = base_url+'admin/dashboard/addlocation';
  }
})
.fail(function() {
  console.log("error");
});


});


  });
</script>