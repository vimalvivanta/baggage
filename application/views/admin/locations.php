<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">
          <h1>
          Location
            
          </h1>

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">locations</a></li>
        
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
         <div class="box box-border">
          <div class="row">
            <div class="col-xs-12">
              <!-- <a class="btn bg-olive btn-flat margin" href="user_acknowledgement">Upload Files</a> -->
              <div class="box-header">
                <h4><button id="add-location" name="add-location" type="button" class="btn btn-primary" >Add New Location</button></h4>
              </div>
              <div class="box-body">
               <!-- Modal Order Status -->
              
                <div class="modal fade" id="mod-requeststatus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div id="requeststatus-result"></div>
                </div>
              </div>
              
                <!-- End Modal Ticket Department-->
                <table id="locations" class="table table-bordered table-striped">
                  <thead>
                    <tr role="row">
                      <th>S.NO</th>
                      <th>Location</th>
                      <th>Date Created</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                <tbody id="loadlocations">
                 
                </tbody>
                </table>
              </div><!-- /.box-body -->
          </div>
        </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>

      $(document ).ready(function() 
{


reload();
   
});
  $(function () {
$('#locations').DataTable();

    $( "#add-location" ).click(function() {

           swal({
  title: "Add location",
  text: "Enter the new loaction ",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",  
  inputPlaceholder: "Location"
},
function(inputValue){
  if (inputValue === false) return false;
  
  if (inputValue === "") {
    swal.showInputError("You need to write something!");
    return false
  }

var base_url='<?php echo base_url();?>';

  $.ajax({
  url: base_url+'admin/dashboard/insertlocation',
  type: 'POST',
  dataType: 'JSON',
  data: {location:inputValue},
})
.done(function(data) {

    if(data.error == false){
       swal("Nice!", "You location: " + inputValue +" is added");
       reload();
    }
  })
.fail(function() {
  console.log("error");
});
  
 
});
  
});



  });


function reload()
{
  var base_url='<?php echo base_url();?>';
  

     $.ajax({    //create an ajax request to load_page.php
        type: 'POST',
        url: base_url+'admin/dashboard/getlocations',             
        dataType: "JSON",   //expect html to be returned   
        data:{form:'getlocations'},            
        success: function(response){                    
    
       
        var records= '';
        var j=0;
          for(var i=0;i<response.length;i++){
                var j=j+1;
                records+='<tr><td>'+j+'</td><td>'+response[i].location+'</td><td>'+response[i].created_on+'</td><td><button class="btn btn-xs btn-warning btn-flat" id="user-inactivate-" onclick="deletelocation('+response[i].location_id+')">Delete</button></td></tr>';
             
           }


          $('#loadlocations').html(records);
        }

});
}

function deletelocation($id)

{
  var base_url='<?php echo base_url();?>';

  $.ajax({
  url: base_url+'admin/dashboard/deletelocation',
  type: 'POST',
  dataType: 'JSON',
  data: {location_id:$id},
})
.done(function(data) {

    if(data == 1){
       swal("deleted!", "You location deleted");
       reload();
    }
  })
.fail(function() {
  console.log("error");
});
  




}
</script>


<script type="text/javascript">

   $(function () {
       //$("#example1").tablesorter();  
       $('#alert-success').delay(5000).fadeOut('slow'); 
       $('#alert-update').delay(5000).fadeOut('slow');     
      });
</script>    