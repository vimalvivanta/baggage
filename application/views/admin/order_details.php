<style>
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}



.widget-user-2 .widget-user-header {
    padding: 29px ;
    
}
.small{
    min-height: 20px;
}
</style>



<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">
          <h1>
           Order Details of <?php echo $orders[0]['order_random_number']; ?> <span class="badge bg-red status">Not Assigned<span></span>
            
          </h1>

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Orders</a></li>
            <li><a href="#">Order Details</a></li>
        
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
         <div class="">
          <div class="row">
          
           <div class="col-md-6">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url(); ?>assets/images/usericon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h4 class="widget-user-username">Name    : <?php echo $orders[0]['user_name']; ?></h4>
              <h4 class="widget-user-username">Email   : <?php echo $orders[0]['user_email']; ?></h4>
              <h4 class="widget-user-username">Mobile  : <?php echo $orders[0]['user_mobile']; ?></h4>
            
            </div>

          </div>



 <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pick Up Address</span>
              <span class="info-box-number"><?php echo $orders[0]['pickup_address']; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Drop  Address</span>
              <span class="info-box-number"><?php echo $orders[0]['drop_address']; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="form-group">
                <label>Select Pick up Driver</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Ram</option>
                  <option>Somu</option>
                  <option disabled="disabled">Ranjan (Assigned )</option>
                  <option>Kumar</option>
                  <option>Ram</option>
                  <option>Raj</option>
                  <option>Rohan</option>
                </select>
              </div>
              <div class="form-group">
                <label>Select Drop Driver</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Lohan</option>
                  <option>Somu</option>
                  <option disabled="disabled">Senthil (Assigned )</option>
                  <option>Kumar</option>
                  <option>Ram</option>
                  <option>Raj</option>
                  <option>Rohan</option>
                </select>
              </div>
 
          <!-- /.widget-user -->
        </div>
           <div class="col-md-3 col-sm-6 col-xs-12">

          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Order Date/Time</span>
              <span class="info-box-number"><?php echo date_format(date_create($orders[0]['created_on']), 'jS F Y, g:i a'); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              
              <span class="info-box-number">No of Bags :<?php echo $orders[0]['no_of_bags'] ?><br>Total Weight: <?php echo $orders[0]['weight_of_bags'] ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box small">
            <!--  <span class="info-box-icon bg-green"><i class="fa fa-envelope-o"></i></span> -->

            <div class="info-box-content">

              <span class="info-box-text">Payment Method</span>
              <span class="info-box-number badge bg-blue ">COD</span>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box small">
            <!--  <span class="info-box-icon bg-green"><i class="fa fa-envelope-o"></i></span> -->

            <div class="info-box-content">

              <span class="info-box-text " >Payment Status</span>
              <span class="info-box-number badge bg-blue">Pending</span>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

                   <div class="col-md-6 ">
                  <div class="box box-widget widget-user-2">
                  <span class="info-box-number">Pickup Driver Details<span>
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url(); ?>assets/images/usericon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h4 class="widget-user-username">Name    : Ram</h4>
              <h4 class="widget-user-username">Email   : ram@gmail.com</h4>
              <h4 class="widget-user-username">Mobile  : 9541415252</h4>
            
            </div>
           
          </div>
                          <div class="box box-widget widget-user-2">
                          <span class="info-box-number">Drop  Driver Details<span>
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-red">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo base_url(); ?>assets/images/usericon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h4 class="widget-user-username">Name    : Lohan</h4>
              <h4 class="widget-user-username">Email   : Lohan222@hotmail.com</h4>
              <h4 class="widget-user-username">Mobile  : 8958574859</h4>
            
            </div>
           
          </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
       
        </div>
        
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>


  $(function () {
$('#locations').DataTable();


  });


// function deleteEmployees($id)
// {
//   var base_url='<?php echo base_url();?>';
//   alert(base_url);
// alert($id);

// swal({
//   title: "Are you sure?",
//   text: "You will not be able to recover this employee details!",
//   type: "warning",
//   showCancelButton: true,
//   confirmButtonColor: "#DD6B55",
//   confirmButtonText: "Yes, delete it!",
//   closeOnConfirm: false
// },
// function($id){
// alert($id);

//      $.ajax({    //create an ajax request to load_page.php
//         type: 'POST',
//         url: base_url+'admin/dashboard/deleteemployee',             
//         dataType: "JSON",   //expect html to be returned   
//         data:{employee_id:$id},            
//         success: function(response){                    
// alert(response);
// return false;
       
    
//         }

// });

// });


// }

function deleteEmployees($id)
{

   var base_url='<?php echo base_url();?>';
     swal({
  title: "Are you sure?",
  text: "You will not be able to recover this employee detail!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "No, cancel!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {



      $.ajax({
  url: base_url+'admin/dashboard/deleteemployee',
  type: 'POST',
  dataType: 'JSON',
  data: {employee_id:$id},
})
.done(function(data) {

                 swal({
           title: "success",
           text: "Employee deleted Successfully",
           type: "success",
           showOkButton: false,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/employees';
          });
              
      
  })
.fail(function() {
  console.log("error");
});
  
  
  } else {
    swal("Cancelled", "Employee detail is safe :)", "error");
  }
});
}

function deletelocation($id)

{
  var base_url='<?php echo base_url();?>';

  $.ajax({
  url: base_url+'admin/dashboard/deletelocation',
  type: 'POST',
  dataType: 'JSON',
  data: {location_id:$id},
})
.done(function(data) {

    if(data == 1){
       swal("deleted!", "You location deleted");
       reload();
    }
  })
.fail(function() {
  console.log("error");
});
  




}
</script>


<script type="text/javascript">

   $(function () {
       //$("#example1").tablesorter();  
       $('#alert-success').delay(5000).fadeOut('slow'); 
       $('#alert-update').delay(5000).fadeOut('slow');     
      });
</script>    