

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
     <div class="pull-right hidden-xs">
        Version <strong> 1.0</strong>
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?php echo date('Y');?> <a href="#">kooly </a>.</strong> All rights reserved.
    </footer>
    
    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    <!-- <script src="<?php echo base_url().'assets/plugins/jQuery/jQuery-2.1.4.min.js'; ?>"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url().'assets/admin/bootstrap/js/bootstrap.min.js'; ?>"></script>
    <!-- Morris.js charts -->
        <!-- // add if you want files from dashboard footes side -->

    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url().'assets/plugins/datepicker/bootstrap-datepicker.js'; ?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url().'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'; ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.min.js'; ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url().'assets/dist/js/app.min.js'; ?>"></script>
    
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url().'assets/dist/js/demo.js'; ?>"></script>
  </body>
</html>
