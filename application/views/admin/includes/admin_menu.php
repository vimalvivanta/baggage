   

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url();?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php  $admindetails= $this->session->userdata('loggedUser');
                echo $admindetails['admin_name'];
               ?></p>
              <a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
         
          <ul class="sidebar-menu">


            <li class="">
              <a href="<?php echo base_url(); ?>/dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa"></i>
              </a>
            </li>
            <!-- <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-dashboard"></i> <span>Blogs</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/create-blog"><i class="fa fa-circle-o"></i> Create Blog</a></li>
                <li><a href="<?php echo base_url(); ?>/blogs-list"><i class="fa fa-circle-o"></i> Blogs List</a></li>
                <li><a href="<?php echo base_url(); ?>/blog-modelistic"><i class="fa fa-circle-o"></i> Blogs modelistic</a></li>
              </ul>
            </li> -->

          <li><a href="<?php echo base_url(); ?>admin/dashboard/site_settings"><i class="fa fa-circle-o"></i>Site Settings</a></li>
      
      <!--     <li><a href="<?php echo base_url(); ?>admin/dashboard/locations"><i class="fa fa-circle-o"></i>Location List</a></li> -->
          <li><a href="<?php echo base_url(); ?>admin/dashboard/employees"><i class="fa fa-circle-o"></i>Employee List</a></li>
          <li><a href="<?php echo base_url(); ?>admin/dashboard/orders"><i class="fa fa-circle-o"></i>Order List</a></li>
           <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-dashboard"></i> <span>Locations</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/admin/dashboard/locations"><i class="fa fa-circle-o"></i>Pick up Location</a></li>
                <li><a href="<?php echo base_url(); ?>/admin/dashboard/locations"><i class="fa fa-circle-o"></i>Drop Location</a></li>
              </ul>
            </li>
         
          <!--   <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-dashboard"></i> <span>Case studies</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/create-casestudies"><i class="fa fa-circle-o"></i> Create Case study</a></li>
                <li><a href="<?php echo base_url(); ?>/casestudies-list"><i class="fa fa-circle-o"></i> Case study List</a></li>
              </ul>
            </li>                 -->         
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-dashboard"></i> <span>Banner Images</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/add-banner-images"><i class="fa fa-circle-o"></i>Create Banner</a></li>
                <li><a href="<?php echo base_url(); ?>/banners-list"><i class="fa fa-circle-o"></i>Banner List</a></li>
              </ul>
            </li>

                                    
           
<!--             <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-pie-chart"></i>
                <span>Charts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-laptop"></i>
                <span>UI Elements</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>/pages/calendar.html">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <small class="label pull-right bg-red">3</small>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>/pages/mailbox/mailbox.html">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <small class="label pull-right bg-yellow">12</small>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-folder"></i> <span>Examples</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                <li><a href="<?php echo base_url(); ?>/pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>/#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>/documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>
            <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="<?php echo base_url(); ?>/#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>