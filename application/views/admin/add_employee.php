<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">

        <?php if($employee_id==''){ ?>
          <h1>
           Add Employee
            
          </h1>
          <?php } else
          {
            ?>
            <h1>
           update Employee
            
          </h1>
            <?php } ?>

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">locations</a></li>
            <li class="active">Add Location</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">

            
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- <div class="box-header with-border">
                  <h3 class="box-title">Quick Example</h3>
                </div> --><!-- /.box-header -->
                <!-- form start -->
                <form id="add-employee-form" role="form" name="add-location-form" action="#" method="POST" >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" class="form-control" id="name" required="" name="name" placeholder="Enter the name of employee" value="<?php echo $employee_details[0]['employee_name']; ?>">
                    

                    </div>

                       <div class="form-group">
                      <label for="email">Email</label>
                      <input type="text" class="form-control" id="email" required="" name="email" placeholder="Enter email" value="<?php echo $employee_details[0]['employee_email']; ?>">
                      <span class="error" id="error-hub"></span>
                 
                    </div>

                       <div class="form-group">
                      <label for="phone">phone</label>
                      <input type="text" class="form-control" id="phone" required="" name="phone" placeholder="Enter mobile number" value="<?php echo $employee_details[0]['employee_mobile']; ?>">
                      <span class="error" id="error-job-location"></span>
                      <input type="hidden" class="form-control" id="employee_id" name="employee_id" value="<?php echo $employee_id; ?>">

                    </div>
              <?php if($employee_id!=''){ ?>

               <div class="form-group">
                      <label for="Hub">Password</label><br>
                     
                       <input type="text" class="form-control" id="password" required="" name="password" placeholder="Leave emptly if you don't want to change password" value="<?php echo base64_decode($employee_details[0]['password']); ?>">
                    </div>

              <?php } ?>

                      
                    <div class="form-group">
                      <label for="Hub">Status</label><br>
                        <input type="radio" id="status" <?php if($employee_details[0]['status']==1){ ?> checked <?php } ?>name="status" value="1"> Active<br>
                        <input type="radio" id="status" <?php if($employee_details[0]['status']==2){ ?> checked <?php } ?> name="status" value="2"> Suspend

                    </div>


<!--                      <div class="form-group">
                      <label for="Hub">Hub to Destination</label>
                      <input type="number" class="form-control" id="hub_to_destination" name="hub_to_destination" required="" placeholder="Enter the distance from Hub to Destination" value="<?php echo $employee_details[0]['employee_name']; ?>">
                      <span class="error" id="hub_to_destination"></span>
                      <input type="hidden" class="form-control" id="hub" name="job-id" value="<?php echo $jobid; ?>">

                    </div>

 -->                  
<!--                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7Glzdi235cx0H8hLXQESse38ASl8lfx4&callback=initMap"
  type="text/javascript"></script>
 -->                  
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <input  id="job-upload-btn" name="job-upload-btn" type="submit" class="btn btn-primary" >
                   <!--  <button id="job-update-btn" name="job-update-btn" type="button" class="btn btn-primary" onclick="addjob(this.id)">Update</button> -->
                <!--   <span id="loading"><img src="<?php echo IMAGES; ?>loading.gif"></span> -->
                    <div id="update-response-message"></div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>
  $(function () {

var base_url ='<?php echo base_url();?>';





$('#add-employee-form').submit(function(e){



if($('#name').val() == ''){
  alert('Please enter the name');
return false;
}else if($('#email').val() == ''){
  alert('Please enter the email');
  return false;
}
else if($('#phone').val() == ''){
  alert('Please enter the phone');
  return false;
}
else if($('#status').val() == ''){
  alert('Please select the staus');
  return false;
}



var a=$('#employee_id').val();

e.preventDefault();



data = $(this).serialize();


if(a=='')
{
  $.ajax({
  url: base_url+'admin/dashboard/insertemployee',
  type: 'POST',
  dataType: 'JSON',
  data: data,
})
.done(function(data) {

if(data==true)
{
   swal({
           title: "success",
           text: "Employee added Successfully",
           type: "success",
           showOkButton: true,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/employees';
          });
}
})
.fail(function() {
  console.log("error");
});
}
else
{

  
  $.ajax({
  url: base_url+'admin/dashboard/insertemployee',
  type: 'POST',
  dataType: 'JSON',
  data: data,
})
.done(function(data) {

if(data==true)
{
   swal({
           title: "success",
           text: "Employee details updated Successfully",
           type: "success",
           showOkButton: true,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/employees';
          });
}
})
.fail(function() {
  console.log("error");
});
}

});


  });
</script>