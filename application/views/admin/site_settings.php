<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">


            <h1>
           Site Settings
            </h1>
     

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Site Settings</a></li>
            
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">

            
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- <div class="box-header with-border">
                  <h3 class="box-title">Quick Example</h3>
                </div> --><!-- /.box-header -->
                <!-- form start -->
                <form id="add-employee-form" role="form" name="add-location-form" action="#" method="POST" >
                  <div class="box-body">
                    <div class="form-group">
                      <label for="name">Price for 1 kg</label>
                      <input type="text" class="form-control" id="one_kg_price" required="" name="one_kg_price" placeholder="One kg price" value="<?php echo $site_settings[0]['price']; ?>">
                    

                    </div>

                       <div class="form-group">
                      <label for="email">Address</label>
                      <input type="text" class="form-control" id="address" required="" name="address" placeholder="Enter Address" value="<?php echo $site_settings[0]['address']; ?>">
                      <span class="error" id="error-hub"></span>
                 
                    </div>

                       <div class="form-group">
                      <label for="phone">Contact Email</label>
                      <input type="text" class="form-control" id="email" required="" name="email" placeholder="Enter email" value="<?php echo $site_settings[0]['email']; ?>">
                      <span class="error" id="error-job-location"></span>
                      <input type="hidden" class="form-control" id="employee_id" name="employee_id" value="<?php echo $employee_id; ?>">

                    </div>
 


                     <div class="form-group">
                      <label for="Hub">Contact number</label>
                      <input type="text" class="form-control" id="contact_number" name="contact_number" required="" placeholder="Enter the Contact Number" value="<?php echo $site_settings[0]['phone']; ?>">
                      <span class="error" id="contact_number"></span>
                      <input type="hidden" class="form-control" id="hub" name="job-id" value="<?php echo $jobid; ?>">

                    </div>

                  
               
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <input  id="job-upload-btn" name="job-upload-btn" type="submit" class="btn btn-primary" >
                   <!--  <button id="job-update-btn" name="job-update-btn" type="button" class="btn btn-primary" onclick="addjob(this.id)">Update</button> -->
                <!--   <span id="loading"><img src="<?php echo IMAGES; ?>loading.gif"></span> -->
                    <div id="update-response-message"></div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>
  $(function () {

var base_url ='<?php echo base_url();?>';






$('#add-employee-form').submit(function(e){



if($('#one_kg_price').val() == ''){
  alert('Please enter the price');
return false;
}else if($('#email').val() == ''){
  alert('Please enter the email');
  return false;
}else if($('#address').val() == ''){
  alert('Please enter the address');
  return false;
}


else if($('#contact_number').val() == ''){
  alert('Please enter the phone number');
  return false;
}



var a=$('#employee_id').val();

e.preventDefault();



data = $(this).serialize();


if(a=='')
{
  $.ajax({
  url: base_url+'admin/dashboard/update_settings',
  type: 'POST',
  dataType: 'JSON',
  data: data,
})
.done(function(data) {
  
if(data==true)
{
   swal({
           title: "success",
           text: "Settings Updated Successfully",
           type: "success",
           showOkButton: true,
          },
          function(){
             //$location.path('/');
            
          });
}
})
.fail(function() {
  console.log("error");
});
}
else
{

  
  $.ajax({
  url: base_url+'admin/dashboard/insertemployee',
  type: 'POST',
  dataType: 'JSON',
  data: data,
})
.done(function(data) {

if(data==true)
{
   swal({
           title: "success",
           text: "Employee details updated Successfully",
           type: "success",
           showOkButton: true,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/employees';
          });
}
})
.fail(function() {
  console.log("error");
});
}

});


  });
</script>