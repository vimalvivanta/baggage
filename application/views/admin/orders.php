<style>
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>



<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <div class="head">
          <h1>
           Employees
            
          </h1>

          </div>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">locations</a></li>
        
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
         <div class="box box-border">
          <div class="row">
            <div class="col-xs-12">
              <!-- <a class="btn bg-olive btn-flat margin" href="user_acknowledgement">Upload Files</a> -->
              <div class="box-header">
                <h4><a href="<?php echo base_url(); ?>admin/dashboard/addemployee"><button id="add-location" name="add-location" type="button" class="btn btn-primary" >Add New Employee</button></a></h4>
              </div>
              <div class="box-body">
               <!-- Modal Order Status -->
              
                <div class="modal fade" id="mod-requeststatus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div id="requeststatus-result"></div>
                </div>
              </div>
              
                <!-- End Modal Ticket Department-->
                <table id="locations" class="table table-bordered table-striped">
                  <thead>
                    <tr role="row">
                      <th>S.NO</th>
                      <th>Customer Name</th>
                      <th>Order Id</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Added On</th>
                      
                      <th>Status</th>
                      <th>Actions</th>
                      
                    </tr>
                  </thead>
                <tbody>
                                <?php $i=0;
                
                    foreach ($orders as $ol) { 
                      // echo "<pre>";print_r($ol);

                    $i++;
                    $bannerid=$ol['id'];
                   if($ol['status']==0)
                   {
                      $status='No action taken';
                   }                 
                   elseif ($ol['status']==1)
                   {
                      $status='Processing';
                   }else
                   {
                     $status='suspend';
                   }    
                  ?>
                    <tr>
                    <td><?php echo $i;?></td>
                    <td> <?php echo $ol['user_name'];?></td>
                      <td> <?php echo $ol['order_random_number'];?></td>
                    <td> <?php echo $ol['user_email'];?></td>
                 
                    <td> <?php echo $ol['user_mobile'];?></td>
                    <td> <?php echo $ol['created_on'];?></td>
                  
                    <td> <?php echo $status;?></td>
                     <td>
                      

   <a href="<?php echo base_url(); ?>admin/dashboard/orderdetails/<?php echo $ol['order_id']; ?>"><button class="btn btn-primary" type="button" >View
  </button></a>

                    </td>
            
                    </tr>
                  <?php } ?>  
                </tbody>
                </table>
              </div><!-- /.box-body -->
          </div>
        </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script>


  $(function () {
$('#locations').DataTable();


  });


// function deleteEmployees($id)
// {
//   var base_url='<?php echo base_url();?>';
//   alert(base_url);
// alert($id);

// swal({
//   title: "Are you sure?",
//   text: "You will not be able to recover this employee details!",
//   type: "warning",
//   showCancelButton: true,
//   confirmButtonColor: "#DD6B55",
//   confirmButtonText: "Yes, delete it!",
//   closeOnConfirm: false
// },
// function($id){
// alert($id);

//      $.ajax({    //create an ajax request to load_page.php
//         type: 'POST',
//         url: base_url+'admin/dashboard/deleteemployee',             
//         dataType: "JSON",   //expect html to be returned   
//         data:{employee_id:$id},            
//         success: function(response){                    
// alert(response);
// return false;
       
    
//         }

// });

// });


// }

function deleteEmployees($id)
{

   var base_url='<?php echo base_url();?>';
     swal({
  title: "Are you sure?",
  text: "You will not be able to recover this employee detail!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "No, cancel!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {



      $.ajax({
  url: base_url+'admin/dashboard/deleteemployee',
  type: 'POST',
  dataType: 'JSON',
  data: {employee_id:$id},
})
.done(function(data) {

                 swal({
           title: "success",
           text: "Employee deleted Successfully",
           type: "success",
           showOkButton: false,
          },
          function(){
             //$location.path('/');
             window.location.href = base_url+'admin/dashboard/employees';
          });
              
      
  })
.fail(function() {
  console.log("error");
});
  
  
  } else {
    swal("Cancelled", "Employee detail is safe :)", "error");
  }
});
}

function deletelocation($id)

{
  var base_url='<?php echo base_url();?>';

  $.ajax({
  url: base_url+'admin/dashboard/deletelocation',
  type: 'POST',
  dataType: 'JSON',
  data: {location_id:$id},
})
.done(function(data) {

    if(data == 1){
       swal("deleted!", "You location deleted");
       reload();
    }
  })
.fail(function() {
  console.log("error");
});
  




}
</script>


<script type="text/javascript">

   $(function () {
       //$("#example1").tablesorter();  
       $('#alert-success').delay(5000).fadeOut('slow'); 
       $('#alert-update').delay(5000).fadeOut('slow');     
      });
</script>    