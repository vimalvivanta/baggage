-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2017 at 07:21 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baggage`
--

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `content_id` int(5) NOT NULL,
  `content` text NOT NULL,
  `type` int(5) NOT NULL,
  `created_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_details`
--

CREATE TABLE `employee_details` (
  `employee_id` int(10) NOT NULL,
  `employee_name` varchar(30) NOT NULL,
  `employee_email` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `employee_mobile` varchar(455) NOT NULL,
  `employee_current_location` varchar(50) NOT NULL,
  `employee_root` int(50) NOT NULL,
  `employee_type` int(5) NOT NULL COMMENT '1- normal users, 2 - admin users',
  `created_on` datetime NOT NULL,
  `status` int(5) NOT NULL COMMENT '1-active,2 - suspend'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_details`
--

INSERT INTO `employee_details` (`employee_id`, `employee_name`, `employee_email`, `password`, `employee_mobile`, `employee_current_location`, `employee_root`, `employee_type`, `created_on`, `status`) VALUES
(1, 'admin', 'admin@kooly.com', 'MTIzNDU=', '3434343434', 'majestic', 1, 2, '2017-04-05 00:00:00', 1),
(8, 'helloooo', 'ssssss@edfs.con', '	a29vbHk=', '45454444444', '', 0, 1, '2017-04-05 06:59:02', 1),
(9, 'dflkjdlgdl', 'FSFSF@DFFD.OCM', 'a29vbHk=', 'SDFDSFSF', '', 0, 1, '2017-04-05 07:25:37', 2),
(12, 'fsnfslfjslf', 'llll@afr.com', '	a29vbHk=', '4545454545', '', 0, 1, '2017-04-05 07:50:22', 1),
(14, 'esfsff', 'kumssaran@gmail.com', '	a29vbHk=', '23232323', '', 0, 1, '2017-04-05 07:53:16', 1),
(16, 'ssfsdf', 'sdfsdfsd@adf.cpm', '	a29vbHk=', '2323232323', '', 0, 1, '2017-04-05 08:04:40', 2),
(17, 'vimal', 'mailtovimalkumar14@gmail.com', '	a29vbHk=', '34343434343', '', 0, 1, '2017-04-05 08:05:10', 2),
(23, 'zczxzx', 'sdfsdfsd@adf.cpm', '	a29vbHk=', '343434', '', 0, 1, '2017-04-05 08:06:01', 1),
(25, 'efwerr', 'wwkumssaran@gmail.com', 'a29vbHkxMjM=', '3434343434', '', 0, 1, '2017-04-05 08:11:11', 1),
(28, 'vimal', 'sdfsdfsd@adf.cpm', '	a29vbHk=', '34343434343', '', 0, 1, '2017-04-05 08:43:50', 1),
(29, '', '', '	a29vbHk=', '', '', 0, 1, '2017-04-06 07:17:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(10) NOT NULL,
  `location` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `location`, `created_on`) VALUES
(5, 'asdad', '2017-04-05 12:11:48'),
(15, 'madurai', '2017-04-05 02:44:42'),
(18, 'chennai', '2017-04-05 02:46:05'),
(20, 'banglore', '2017-04-05 03:45:53'),
(21, 'karnataka', '2017-04-05 03:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_mobile` varchar(25) NOT NULL,
  `email_verification` int(5) NOT NULL,
  `mobile_verification` int(5) NOT NULL,
  `pickup_id` int(10) NOT NULL,
  `drop_id` int(10) NOT NULL,
  `pickup_address` varchar(400) NOT NULL,
  `drop_address` varchar(400) NOT NULL,
  `total_distance` varchar(40) NOT NULL,
  `total_price` varchar(40) NOT NULL,
  `otp` int(20) NOT NULL,
  `order_random_number` int(30) NOT NULL,
  `status` int(5) NOT NULL COMMENT '1 - paid , 2 - processing ,3 - delivered',
  `pick_up_driver` int(10) NOT NULL,
  `drop_driver` int(10) NOT NULL,
  `pick_up_from_user` int(5) NOT NULL,
  `drop_to_hub` int(5) NOT NULL,
  `pick_up_from_hub` int(5) NOT NULL,
  `drop_to_user` int(5) NOT NULL,
  `no_of_bags` int(10) NOT NULL,
  `weight_of_bags` int(10) NOT NULL,
  `type_of_payment` int(10) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_name`, `user_email`, `user_mobile`, `email_verification`, `mobile_verification`, `pickup_id`, `drop_id`, `pickup_address`, `drop_address`, `total_distance`, `total_price`, `otp`, `order_random_number`, `status`, `pick_up_driver`, `drop_driver`, `pick_up_from_user`, `drop_to_hub`, `pick_up_from_hub`, `drop_to_user`, `no_of_bags`, `weight_of_bags`, `type_of_payment`, `created_on`) VALUES
(1, 'vimal', 'vimal@gmail.com', '34334434343', 1, 1, 0, 0, 'silkboard', 'kamatchi palya', '13', '49', 784746, 888888, 0, 0, 0, 0, 0, 0, 0, 2, 50, 1, '2017-04-06 03:00:00'),
(2, 'kumar', 'kumar@gmail.com', '3434343443', 1, 1, 0, 0, 'koramangala', 'kamatchi palya', '', '', 784747, 888882, 0, 0, 0, 0, 0, 0, 0, 2, 50, 1, '2017-04-06 00:00:00'),
(3, 'sssss', 'sssss@gmail.com', '453545345878', 1, 1, 0, 0, 'koramangala', 'kamatchi palya', '', '', 784744, 988888, 0, 0, 0, 0, 0, 0, 0, 2, 50, 1, '2017-04-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateway`
--

CREATE TABLE `payment_gateway` (
  `payment_id` int(10) NOT NULL,
  `payment_name` varchar(40) NOT NULL,
  `payment_key` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_transactions`
--

CREATE TABLE `payment_transactions` (
  `payment_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `amount` float(4,3) NOT NULL,
  `status` int(5) NOT NULL,
  `gateway` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roots`
--

CREATE TABLE `roots` (
  `root_id` int(5) NOT NULL,
  `start_place` varchar(20) NOT NULL,
  `end_place` varchar(20) NOT NULL,
  `hub` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `price` float(10,4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `address`, `price`, `email`, `phone`) VALUES
(1, 'sdf skflsk fsljksls fsa asd sdfs', 60.0000, 'admin@kooly.comg', '87787878787');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `employee_details`
--
ALTER TABLE `employee_details`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `roots`
--
ALTER TABLE `roots`
  ADD PRIMARY KEY (`root_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `content_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_details`
--
ALTER TABLE `employee_details`
  MODIFY `employee_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roots`
--
ALTER TABLE `roots`
  MODIFY `root_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
